public class MethodsTest {
  public static void main (String[] args) {
    int x = 10;
	System.out.println(x);
    methodNoInputNoReturn(10);
	methodNoInputNoReturn(x);
	methodNoInputNoReturn(x+50);
	
	methodTwoInputNoReturn(5, 1.1);
	
	int z = methodNoInputNoReturnInt();
	System.out.println(z);
	
	System.out.println(sumSquareRoot(6, 3));
	
	String s1 = "hello";
	String s2 = "goodbye";
	System.out.println(s1.length());
	System.out.println(s2.length());
	
	SecondClass sc = new SecondClass();
	System.out.println(SecondClass.addOne(50));
	int two = sc.addTwo(50);
	
  }
  
  public static void methodNoInputNoReturn (int cool) {
    System.out.println("I'm in a method that takes no input and returns nothing"); 
    int x = 50;    
    System.out.println(x);
	System.out.println("Inside the method one input no return");
	System.out.println(cool);
	}
  
  public static void methodTwoInputNoReturn (int one, double two) {
	System.out.println(one);
	System.out.println(two);
  
}

  public static int methodNoInputNoReturnInt () {
	
	return 6;
}
  
  public static double sumSquareRoot(int one, int two) {
	double three = one + two;
	three = Math.sqrt(three);
	return three;
	  
  }

}
    